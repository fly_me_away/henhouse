var path = require('path');
var gulp = require('gulp');
var sass = require('gulp-sass');		// Sass complied
var sourcemaps = require('gulp-sourcemaps');	// Source maps
var rename = require('gulp-rename'); 	// Rename


var ROOT_PATH = path.resolve(__dirname);
var SASS_PATH = path.resolve(ROOT_PATH,'build/static/sass');
var OUTPUT_CSS_PATH = path.resolve(ROOT_PATH,'public/src/css');
// == Sass complied
gulp.task('sass',function(){
    return gulp.src(SASS_PATH+'/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error',sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(OUTPUT_CSS_PATH+'/'));
});

// == Create css compressed
gulp.task('sass:min',function () {
    return gulp.src(SASS_PATH+'/**/*.scss')
        .pipe(rename({
            suffix:".min"
        }))
        .pipe(sass({outputStyle:'compressed'}).on('error',sass.logError))
        .pipe(gulp.dest(OUTPUT_CSS_PATH+'/'));
});

// == Watch sass changed
gulp.task('sass:watch',function () {
    gulp.watch(SASS_PATH+'/**/*.scss',['sass','sass:min']);
});

// == Create js compressed
gulp.task('js:min',function(){
    pump([
        gulp.src('./dest/js/shareh5.js').pipe(rename({
            suffix:".min"
        })),
        uglify(),
        gulp.dest('./dest/js/')
    ]);
});

// == Watch js changed
gulp.task('js:watch',function () {
    gulp.watch('./dest/js/shareh5.js',['js:min']);
});

// == Default
gulp.task('default',['sass:watch','js:watch']);