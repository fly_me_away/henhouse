/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	var Api = __webpack_require__(1);
	var WindowController = __webpack_require__(2);
	var ToolsController = __webpack_require__(3);
	var FormController = __webpack_require__(4);

	window.Hen = {
	        WindowController:WindowController,
	        ToolsController:ToolsController,
	        Form:FormController,
	        Api:Api
	    };

	// *************** 公共方法 **************** //
	// == 当发起ajax请求前添加加载器
	$(document).ajaxSend(function(event, request, settings) {

	});
	// == 当ajax请求完成后关闭加载器
	$(document).ajaxComplete(function(event, request, settings) {
	});
	// == 网络请求错误提示
	$(document).ajaxError(function(event, request, settings) {
	   // window.location.href = "../../page/404.html";
	});
	// *************** 模态框公用方法 **************//
	// *************** 表单公用方法 ***************//
	$(document).ready(function () {
	    $('.create-date').val(ToolsController.getDate().date);
	});


/***/ },
/* 1 */
/***/ function(module, exports) {

	var API = {
	    url:{
	        //hostname:'182.92.111.21',
	        hostname:'192.168.0.23',
	        port: 8080,
	        headers: {
	            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
	        }
	    },
	    config:{
	        key:'test',
	        code:'1234567890',
	        v:'1.0',
	        format:'json'
	    },
	    sign:{
	        key:'test',
	        code:'1234567890',
	        v:'1.0',
	        format:'json'
	    },
	    api:{
	        sign:"/sign/rest/1.0/getService",
	        login:"/farmerlogin/login/rest/1.0/getService",
	        henhouse:{
	            add:"/web/chicken/add/rest/1.0/getService",
	            list:"/web/chicken/list/rest/1.0/getService",
	            edit:"/web/chicken/find/rest/1.0/getService",
	            update:"/web/chicken/update/rest/1.0/getService",
	            delete:"/web/chicken/delete/rest/1.0/getService",
	            intro:"/web/chicken/synopsis/rest/1.0/getService",
	            detail:"/web/chicken/chickenDetail/rest/1.0/getService",
	            message:"/web/chicken/importantMessage/rest/1.0/getService",
	            controlList:"/web/control/list/rest/1.0/getService",
	            controlAdd:"/web/control/add/rest/1.0/getService",
	            controlSave:"/web/control/save/rest/1.0/getService",
	            controlView:"/web/control/edit/rest/1.0/getService",
	            controlDelete:"/web/control/delete/rest/1.0/getService",
	            controlUpdate:"/web/control/update/rest/1.0/getService",
	            form:{
	                hencoopNum:"/web/chicken/addChickenDataCheck/rest/1.0/getService"
	            }
	        },
	        breeder:{
	            add:"/web/breeder/add/rest/1.0/getService",
	            save:"/web/breeder/rest/1.0/getService",
	            list:"/web/breeder/rest/1.0/getService",
	            delete:"/web/breeder/rest/1.0/getService",
	            view:"/web/breeder/view/rest/1.0/getService",
	            form:{
	                username:"/check/breeder/username/rest/1.0/getService"
	            }
	        },
	        member:{
	            save:"/web/member/addMemberInfo/rest/1.0/getService",
	            list:"/web/member/getMemberList/rest/1.0/getService",
	            delete:"/web/member/deleteMemberInfo/rest/1.0/getService",
	            view:"/web/member/seeMemberInfo/rest/1.0/getService",
	            addhenhouse:"/web/member/getHenhouseList/rest/1.0/getService"
	        },
	        varieties:{
	            add:"/web/farmInfo/addChickenVarieties/rest/1.0/getService",
	            list:"/web/farmInfo/varietiesInfo/rest/1.0/getService",
	            edit:"/web/farmInfo/getChickenVariety/rest/1.0/getService",
	            update:"/web/farmInfo/editChickenVarieties/rest/1.0/getService",
	            delete:"/web/farmInfo/deleteChickenVarieties/rest/1.0/getService",
	            form:{
	                varietyname:"/check/variety/username/rest/1.0/getService"
	            }
	        },
	        my:{
	            update:"/web/farmerInfo/updatefarmerInfo/rest/1.0/getService",
	            password:"/web/farmerInfo/modifyfarmerInfo/rest/1.0/getService"
	        },
	        work:{
	            add:"/web/work/save/rest/1.0/getService",
	            list:"/web/work/list/rest/1.0/getService",
	            edit:"/web/work/edit/rest/1.0/getService",
	            update:"/web/work/update/rest/1.0/getService",
	            distribute:"/web/work/distribution/rest/1.0/getService",
	            delete:"/web/work/delete/rest/1.0/getService",
	            recode:"/web/work/breederWork/rest/1.0/getService",
	            detail:"/web/work/workDetails/rest/1.0/getService"
	        },
	        chart:{
	            chickenCount:"/web/statistics/getIndex/rest/1.0/getService", // 农场小鸡总数统计图
	            chickenEgg:"/web/statistics/eggnumberbyfarm/rest/1.0/getService", // 农场小鸡产蛋量统计图
	            foodAndWater:"/web/statistics/foodandwater/rest/1.0/getService", // 鸡舍粮水统计图
	            temperature:"/web/statistics/temperature/rest/1.0/getService", //鸡舍温度统计图
	            humidity:"/web/statistics/humidity/rest/1.0/getService", // 鸡舍湿度统计图
	            egg:"/web/statistics/eggNumberByHencoop/rest/1.0/getService", // 鸡舍产蛋统计
	            farmFood:"/web/statistics/foodandwaterbyfarm/rest/1.0/getService", // 农场粮水统计图
	            farmEgg:"/web/statistics/eggnumberbyfarm/rest/1.0/getService", // 农场产蛋统计图
	            farmBuy:"/web/statistics/buyChickenByfarm/rest/1.0/getService", // 农场小鸡买入统计图
	            farmDie:"/web/statistics/deathChickenByfarm/rest/1.0/getService", // 农场小鸡死亡统计图
	            farmSell:"/web/statistics/sellChickenByfarm/rest/1.0/getService", // 农场小鸡卖出统计图
	            chickenNum:"/web/statistics/chickennumberbyfarm/rest/1.0/getService" //小鸡数量
	        },
	        images:{
	            banner:"/web/picture/carouselSave/rest/1.0/getService"
	        },
	        other:{
	            messages:"/web/statistics/messageByfarm/rest/1.0/getService",
	            about:"/web/farmInfo/aboutUs/rest/1.0/getService",
	            aboutView:"/web/chicken/aboutUs/rest/1.0/getService",
	            breederList:"/web/chicken/breederList/rest/1.0/getService",
	            feedback:"/web/farmInfo/feedbackInfo/rest/1.0/getService",
	            feedbackDelete:"/web/farmInfo/deleteFeedbackById/rest/1.0/getService"
	        }
	    }
	};
	module.exports = API;

/***/ },
/* 2 */
/***/ function(module, exports) {

	var WindowController = (function ($) {
	    var setIframeSize = function () {
	        var getWindowWidth = $(window).width();
	        var getWindowHeight = $(window).height();
	        return{
	            getWindowWidth:getWindowWidth,
	            getWindowHeight:getWindowHeight
	        }
	    };

	    return{
	        setIframeSize:setIframeSize
	    }
	})(jQuery);
	module.exports = WindowController;


/***/ },
/* 3 */
/***/ function(module, exports) {

	var ToolsController = (function ($) {
	    var getDate = function () {
	        var monthNames = ["1月", "2月", "3月", "4月", "5月", "6月","7月", "8月", "9月", "10月", "11月", "12月"];
	        var dayNames = ["星期日, ", "星期一, ", "星期二, ", "星期三, ", "星期四, ", "星期五, ", "星期六, "];
	        var newDate = new Date();
	        newDate.setDate(newDate.getDate());
	        return {
	            week:dayNames[newDate.getDay()],
	            date:newDate.getFullYear() + '年 ' + monthNames[newDate.getMonth()] + ' ' + newDate.getDate()+'日',
	            weekAndDate:dayNames[newDate.getDay()] + " " + newDate.getFullYear() + '年 ' + monthNames[newDate.getMonth()] + ' ' + newDate.getDate()+'日'
	        }
	    };

	    var createDate = function (date) {
	        return moment(date).format('L');
	    };

	    var getToday = function () {
	        var today = new Date();
	        return today.getFullYear()+"-"+(today.getMonth()+1)+"-"+today.getDate();
	    };

	    // == 获取url中的参数值
	    /*
	     * @param name String /detail.html?topicId=4
	     * */
	    var getQueryString = function (name) {
	        var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
	        var r = window.location.search.substr(1).match(reg);
	        if (r != null) {
	            return decodeURIComponent(r[2]);
	        }
	        return null;
	    };

	    /*
	     * @param images String /图片地址为null，转换为默认图片地址
	     * */
	    var textDefault = function (text,str) {
	        if(text == null || text == ''){
	            if(str){
	                return str;
	            }else{
	                return "";
	            }
	        }else{
	            return text;
	        }
	    };

	    return{
	        getDate:getDate,
	        createDate:createDate,
	        getQueryString:getQueryString,
	        textDefault:textDefault,
	        getToday:getToday
	    }
	})(jQuery);
	module.exports = ToolsController;


/***/ },
/* 4 */
/***/ function(module, exports) {

	var FormController = (function ($) {
	    var cookieTime = function (){
	        end = new Date();
	        end = new Date(end.valueOf() + 7*24*60*60*1000);
	        return end;
	    };
	    return{
	        cookieTime:cookieTime
	    }
	})(jQuery);
	module.exports = FormController;


/***/ }
/******/ ]);