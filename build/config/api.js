var API = {
    url:{ // API服务器地址
        hostname:'182.92.111.21',
        //hostname:'192.168.0.40',
        port: 8087,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }
    },
    config:{ // http请求配置
        key:'test',
        code:'1234567890',
        v:'1.0',
        format:'json'
    },
    sign:{ // 用户数据签名
        key:'test',
        code:'1234567890',
        v:'1.0',
        format:'json'
    },
    api:{
        sign:"/sign/rest/1.0/getService", // 数据签名
        login:"/farmerlogin/login/rest/1.0/getService", // 用户登录
        henhouse:{
            add:"/web/chicken/add/rest/1.0/getService", // 鸡舍添加
            list:"/web/chicken/list/rest/1.0/getService", // 鸡舍列表
            edit:"/web/chicken/find/rest/1.0/getService", // 鸡舍编辑
            update:"/web/chicken/update/rest/1.0/getService", // 鸡舍更新
            delete:"/web/chicken/delete/rest/1.0/getService", // 鸡舍删除
            intro:"/web/chicken/synopsis/rest/1.0/getService", // 鸡舍简介
            detail:"/web/chicken/chickenDetail/rest/1.0/getService", // 鸡舍详情
            message:"/web/chicken/importantMessage/rest/1.0/getService", // 鸡舍重要信息
            controlList:"/web/control/list/rest/1.0/getService", // 鸡舍控制列表
            controlAdd:"/web/control/add/rest/1.0/getService", // 添加鸡舍控制流程
            controlSave:"/web/control/save/rest/1.0/getService", // 保存添加鸡舍控制流程
            controlView:"/web/control/edit/rest/1.0/getService", // 查看鸡舍控制流程
            controlDelete:"/web/control/delete/rest/1.0/getService", // 删除鸡舍控制流程
            controlUpdate:"/web/control/update/rest/1.0/getService", // 更新鸡舍控制流程
            exportCard:"/web/resume/export/rest/1.0/getService", // 鸡舍导出名片
            form:{
                hencoopNum:"/web/chicken/addChickenDataCheck/rest/1.0/getService", // 鸡舍编号数据校验
                hardware:"/web/chicken/addChickenHardWareIdCheck/rest/1.0/getService"
            }
        },
        breeder:{
            add:"/web/breeder/add/rest/1.0/getService", // 添加饲养员
            save:"/web/breeder/rest/1.0/getService", // 保存饲养员
            list:"/web/breeder/rest/1.0/getService", // 饲养员列表
            delete:"/web/breeder/rest/1.0/getService", // 删除饲养员
            view:"/web/breeder/view/rest/1.0/getService", // 查看饲养员
            form:{
                username:"/check/breeder/username/rest/1.0/getService" // 饲养员用户名数据校验
            }
        },
        member:{
            save:"/web/member/addMemberInfo/rest/1.0/getService", // 保存会员
            list:"/web/member/getMemberList/rest/1.0/getService", // 会员列表
            delete:"/web/member/deleteMemberInfo/rest/1.0/getService", // 删除会员
            view:"/web/member/seeMemberInfo/rest/1.0/getService", // 查看会员
            addhenhouse:"/web/member/getHenhouseList/rest/1.0/getService" // 会员绑定鸡舍
        },
        varieties:{
            add:"/web/farmInfo/addChickenVarieties/rest/1.0/getService", // 添加品种
            list:"/web/farmInfo/varietiesInfo/rest/1.0/getService", // 品种列表
            edit:"/web/farmInfo/getChickenVariety/rest/1.0/getService", // 编辑品种
            update:"/web/farmInfo/editChickenVarieties/rest/1.0/getService", // 更新品种
            delete:"/web/farmInfo/deleteChickenVarieties/rest/1.0/getService", // 删除品种
            form:{
                varietyname:"/check/variety/username/rest/1.0/getService" // 品种名称数据校验
            }
        },
        my:{
            update:"/web/farmerInfo/updatefarmerInfo/rest/1.0/getService", // 更新我的信息
            password:"/web/farmerInfo/modifyfarmerInfo/rest/1.0/getService" // 修改密码
        },
        work:{
            add:"/web/work/save/rest/1.0/getService", // 添加工作记录
            list:"/web/work/hencoopworkitmes/rest/1.0/getService", // 工作记录列表
            edit:"/web/work/edit/rest/1.0/getService", // 编辑工作记录
            update:"/web/work/update/rest/1.0/getService", // 更新工作记录
            distribute:"/web/work/distribution/rest/1.0/getService", // 分配工作记录
            delete:"/web/work/delete/rest/1.0/getService", // 删除工作记录
            recode:"/web/work/breederWork/rest/1.0/getService", // 工作记录
            detail:"/web/work/workDetails/rest/1.0/getService", // 工作记录详情
            excel:"/web/work/hencoopworkitmesexecle/rest/1.0/getService", // 工作记录导出Excel
            addTask:"/web/work/workDetails/rest/1.0/getService", // 添加工作任务
            listTask:"/web/work/breedertemporarytasks/rest/1.0/getService", //工作任务列表
            editTask:"/web/work/updatebreedertemporarytask/rest/1.0/getService", // 编辑工作任务
            deleteTask:"/web/work/deletebreedertemporarytask/rest/1.0/getService", // 删除临时任务
            sendTask:"/web/work/sendbreedertemporarytask/rest/1.0/getService" // 发送临时任务
        },
        chart:{
            chickenCount:"/web/statistics/getIndex/rest/1.0/getService", // 农场小鸡总数统计图
            chickenEgg:"/web/statistics/eggnumberbyfarm/rest/1.0/getService", // 农场小鸡产蛋量统计图
            foodAndWater:"/web/statistics/foodandwater/rest/1.0/getService", // 鸡舍粮水统计图
            temperature:"/web/statistics/temperature/rest/1.0/getService", //鸡舍温度统计图
            humidity:"/web/statistics/humidity/rest/1.0/getService", // 鸡舍湿度统计图
            egg:"/web/statistics/eggNumberByHencoop/rest/1.0/getService", // 鸡舍产蛋统计
            farmFood:"/web/statistics/foodandwaterbyfarm/rest/1.0/getService", // 农场粮水统计图
            farmEgg:"/web/statistics/eggnumberbyfarm/rest/1.0/getService", // 农场产蛋统计图
            farmBuy:"/web/statistics/buyChickenByfarm/rest/1.0/getService", // 农场小鸡买入统计图
            farmDie:"/web/statistics/deathChickenByfarm/rest/1.0/getService", // 农场小鸡死亡统计图
            farmSell:"/web/statistics/sellChickenByfarm/rest/1.0/getService", // 农场小鸡卖出统计图
            chickenNum:"/web/statistics/chickennumberbyfarm/rest/1.0/getService", //小鸡数量
            eggFood:"/web/chicken/eggfeedcolumn/rest/1.0/getService", //农场蛋料比统计图
            dieOut:"/web/chicken/deathsscolumn/rest/1.0/getService", //农场死淘率统计图
            temHum:"/web/chicken/temperature/rest/1.0/getService" //农场温度统计图
        },
        images:{
            bannerView:"/web/picture/carouselFind/rest/1.0/getService", // 查看轮播图
            banner:"/web/picture/carouselSave/rest/1.0/getService", // 添加轮播图
            bannerDelete:"/web/picture/deleteCarouselById/rest/1.0/getService", // 删除轮播图
            hencoopList:"/web/Camera/hencoopList/rest/1.0/getService", // 鸡舍每日图片
            hencoopCameras:"/web/Camera/hencoopCameras/rest/1.0/getService", // 鸡舍监控
            cameraImages:"/web/Camera/cameraImages/rest/1.0/getService", // 监控图片
            check:"/web/brandfeedtesting/updateTesting/rest/1.0/getService", // 检测报告图片上传
            checkShow:"/web/brandfeedtesting/editTesting/1.0/getService" // 检测报告回显
        },
        camera:{
            addCamera:"/web/Camera/addCamera/rest/1.0/getService", // 添加摄像头
            deleteCamera:"/web/Camera/deleteCamera/rest/1.0/getService", // 删除摄像头
            viewCamera:"/web/Camera/seeCamera/rest/1.0/getService", // 查看摄像头
            updateCamera:"/web/Camera/updateCamera/rest/1.0/getService" // 更新摄像头
        },
        exhibition:{
            list:"/web/exhibition/exhibitionList/rest/1.0/getService", // 展示列表
            add:"/web/exhibition/addExhibition/rest/1.0/getService", // 添加展示信息
            del:"/web/exhibition/deleteExhibition/rest/1.0/getService", //删除展示信息
            editShow:"/web/exhibition/edit/rest/1.0/getService", // 编辑展示回显
            update:"/web/exhibition/updateExhibition/rest/1.0/getService" //修改展示信息
        },
        environment:{
            list:"/web/environment/environmentList/rest/1.0/getService", // 环境列表
            add:"/web/environment/addEnvironment/rest/1.0/getService", // 添加环境
            del:"/web/environment/deleteEnvironment/rest/1.0/getService",// 删除环境信息
            editShow:"/web/environment/edit/rest/1.0/getService",// 编辑环境回显
            update:"/web/environment/updateEnvironment/rest/1.0/getService" // 修改环境信息
        },
        brand:{
            add:"/web/brandfeedtesting/updateBrand/rest/1.0/getService", // 品牌介绍
            view:"/web/brandfeedtesting/editBrand/1.0/getService" // 品牌简介回显
        },
        feed:{
            add:"/web/brandfeedtesting/updateFeed/rest/1.0/getService", // 饲料信息
            view:"/web/brandfeedtesting/editFeed/1.0/getService" // 饲料信息回显
        },
        other:{
            messages:"/web/statistics/messageByfarm/rest/1.0/getService", // 重要信息
            about:"/web/farmInfo/aboutUs/rest/1.0/getService", // 添加关于我们
            aboutView:"/web/chicken/aboutUs/rest/1.0/getService", // 查看关于我们
            breederList:"/web/chicken/breederList/rest/1.0/getService", // 饲养员下拉列表
            feedback:"/web/farmInfo/feedbackInfo/rest/1.0/getService", // 意见反馈列表
            feedbackDelete:"/web/farmInfo/deleteFeedbackById/rest/1.0/getService", // 删除意见反馈
            downRecode:"/web/resume/batchGenerate/rest/1.0/getService", // 履历批量下载
            clearCheck:"/web/brandfeedtesting/clearTesting/1.0/getService", // 清除检测报告
            comment:""
        }
    }
};
module.exports = API;