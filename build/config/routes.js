var path = require('path');
var LoginController = require('../controller/LoginController');
var MainController = require('../controller/MainController');
var IndexController = require('../controller/IndexController');
var BreederController = require('../controller/BreederController');
var MemberController = require('../controller/MemberController');
var MyController = require('../controller/MyController');
var VarietiesController = require('../controller/VarietiesController');
var AboutController = require('../controller/AboutController');
var HenhouseController = require('../controller/HenhouseController');
var WorkController = require('../controller/WorkController');
var ChartController = require('../controller/ChartController');
var ImagesController = require('../controller/ImagesController');
var ExhibitionController = require('../controller/ExhibitionController');
var BrandController = require('../controller/BrandController');
var FeedController = require('../controller/FeedController');
var EnvironmentController = require('../controller/EnvironmentController');
var RecodeController = require('../controller/RecodeController');
module.exports = function (app) {
    // == 登录页
    app.get('/login',LoginController.loginPage);
    // == 登录
    app.post('/login',LoginController.login);
    // == 注销
    app.get('/logout',LoginController.logout);
    // == 框架页面
    app.get('/',LoginController.signinRequire,MainController.mainPage);
    // == 首页
    app.get('/index',LoginController.signinRequire,IndexController.indexPage);
    // == 首页重要信息
    app.post('/messages',IndexController.messagesInfo);

    /*************************************鸡舍管理*****************************************/
    // == 鸡舍列表
    app.get('/henhouse/list/:page/*',LoginController.signinRequire,HenhouseController.listPage);
    // == 鸡舍统计
    app.get('/henhouse/statistics',LoginController.signinRequire,HenhouseController.statisticsPage);
    // == 添加鸡舍
    app.post('/henhouse/add',HenhouseController.addHenhouse);
    // == 查看简介
    app.post('/henhouse/intro',HenhouseController.introHenhouse);
    // == 查看详情
    app.get('/henhouse/detail/*',LoginController.signinRequire,HenhouseController.detailHenhouse);
    // == 导出名片
    app.post('/henhouse/exportCard',HenhouseController.exportCard);
    // == 获取编辑信息
    app.post('/henhouse/edit',HenhouseController.editHenhouse);
    // == 更新鸡舍
    app.post('/henhouse/update',HenhouseController.updateHenhouse);
    // == 删除鸡舍
    app.post('/henhouse/delete',HenhouseController.deleteHenhouse);
    // == 鸡舍重要信息
    app.post('/henhouse/messages',HenhouseController.messagesHenhouse);
    // == 鸡舍控制
    app.get('/henhouse/control/:page/',LoginController.signinRequire,HenhouseController.controlPage);
    // == 鸡舍控制列表
    app.get('/henhouse/control/list/*',LoginController.signinRequire,HenhouseController.controlListPage);
    // == 添加鸡舍控制流程
    app.get('/control/add/*',LoginController.signinRequire,HenhouseController.addControl);
    // == 保存鸡舍控制流程
    app.post('/control/save',HenhouseController.saveControl);
    // == 查看鸡舍控制流程
    app.get('/control/view/*',LoginController.signinRequire,HenhouseController.viewControl);
    // == 编辑鸡舍控制流程
    app.get('/control/edit/*',LoginController.signinRequire,HenhouseController.editControl);
    // == 获取控制数据
    app.post('/control/ajax',LoginController.signinRequire,HenhouseController.ajaxControl);
    // == 更新鸡舍控制流程
    app.post('/control/update',HenhouseController.updateControl);
    // == 删除鸡舍控制流程
    app.delete('/control/delete',HenhouseController.deleteControl);
    // == 添加鸡舍编号数据校验
    app.post('/henhouse/form/hencoopnum',HenhouseController.hencoopNumForm);
    // == 添加鸡舍硬件ID数据校验
    app.post('/henhouse/form/hardwareid',HenhouseController.hardwareIdForm);
    // == 绑定鸡舍摄像头
    app.post('/henhouse/camera/add',HenhouseController.addCamera);
    // == 删除鸡舍摄像头
    app.post('/henhouse/camera/delete',HenhouseController.deleteCamera);
    // == 查看鸡舍摄像头
    app.post('/henhouse/camera/view',HenhouseController.viewCamera);
    // == 更新鸡舍摄像头
    app.post('/henhouse/camera/update',HenhouseController.updateCamera);

    /*************************************饲养员管理***************************************/
    // == 饲养员列表
    app.get('/breeder/list/:page',LoginController.signinRequire,BreederController.listPage);
    // == 饲养员列表分页
    //app.post('/breeder/list',LoginController.signinRequire,BreederController.listPagination);
    // == 删除饲养员
    app.delete('/breeder/list',LoginController.signinRequire,BreederController.deleteBreeder);
    // == 查看饲养员
    app.get('/breeder/view/:id',LoginController.signinRequire,BreederController.viewBreeder);
    // == 添加饲养员
    app.post('/breeder/add',BreederController.addBreeder);
    // == 保存饲养员
    app.post('/breeder/save',ImagesController.uploadImage,BreederController.saveBreeder);
    // == 饲养员表单验证
    app.post('/breeder/form/username',BreederController.formValidity);
    // == 工作日程
    app.get('/work/schedule/:page/',LoginController.signinRequire,WorkController.schedulePage);
    // == 添加工作日程
    app.post('/work/schedule/add',WorkController.addSchedule);
    // == 编辑工作日程
    app.post('/work/schedule/edit',WorkController.editSchedule);
    // == 更新工作日程
    app.post('/work/schedule/update',WorkController.updateSchedule);
    // == 工作日程分配
    app.get('/work/distribution/:page/*',LoginController.signinRequire,WorkController.distributePage);
    // == 确认分配
    app.post('/work/distribution/confirm',WorkController.confirmDistribution);
    // == 删除日程
    app.delete('/work/schedule',WorkController.deleteSchedule);
    // == 工作记录
    //app.get('/work/recode/:page',LoginController.signinRequire,WorkController.recodePage);
    // == 工作记录详情
    app.get('/work/recode/:page/*',LoginController.signinRequire,WorkController.recodePage);
    // == 工作记录导出Excel
    app.post('/work/detail/excel',WorkController.exportExcel);
    // == 工作任务
    app.get('/work/task/:page',LoginController.signinRequire,WorkController.tasksPage);
    // == 添加工作任务
    app.post('/work/task/add',LoginController.signinRequire,WorkController.addTask);
    // == 编辑工作任务
    app.post('/work/task/edit',LoginController.signinRequire,WorkController.editTask);
    // == 删除工作任务
    app.post('/work/task/delete',LoginController.signinRequire,WorkController.deleteTask);
    // == 发送工作任务
    app.post('/work/task/send',LoginController.signinRequire,WorkController.sendTask);

    /***********************************会员管理*****************************************/
    // == 会员列表
    app.get('/member/list/:page/*',LoginController.signinRequire,MemberController.listPage);
    // == 添加会员
    app.get('/member/add',LoginController.signinRequire,MemberController.addMember);
    // == 会员添加鸡舍
    app.get('/member/add/henhouse/:page/*',LoginController.signinRequire,MemberController.addMemberHenhouse);
    // == 保存添加会员
    app.post('/member/save',MemberController.saveMember);
    // == 删除会员
    app.delete('/member/list',LoginController.signinRequire,MemberController.deleteMember);
    // == 查看会员
    app.get('/member/view/:id',LoginController.signinRequire,MemberController.viewMember);
    // == 编辑会员
    app.get('/member/edit/*',LoginController.signinRequire,MemberController.editMember);

    /**************************************农场管理**************************************/
    // == 品种信息，品种列表
    app.get('/varieties/list',LoginController.signinRequire,VarietiesController.listPage);
    // == 品种列表分页
    app.post('/varieties/list',LoginController.signinRequire,VarietiesController.listPagination);
    // == 编辑品种信息
    app.post('/varieties/edit',LoginController.signinRequire,VarietiesController.editVariety);
    // == 更新品种信息
    app.post('/varieties/update',LoginController.signinRequire,VarietiesController.updateVariety);
    // == 添加品种信息
    app.post('/varieties/add',LoginController.signinRequire,VarietiesController.addVariety);
    // == 删除品种
    app.delete('/varieties/list',VarietiesController.deleteVariety);
    // == 品种信息验证
    app.post('/varieties/form/varietyname',VarietiesController.formValidity);
    // == 关于我们
    app.get('/about',LoginController.signinRequire,AboutController.aboutPage);
    // == 关于我们提交数据
    app.post('/about',AboutController.setAboutContent);
    // == 意见反馈
    app.get('/feedback/:page',LoginController.signinRequire,AboutController.feedbackPage);
    // == 删除意见反馈
    app.delete('/feedback/list',AboutController.feedbackDelete);
    // == 图片
    app.get('/image',LoginController.signinRequire,ImagesController.imagePage);
    // == 轮播图片
    app.get('/images/720',LoginController.signinRequire,ImagesController.overallPage);
    // == 全局图片
    app.get('/images/global',LoginController.signinRequire,ImagesController.globalPage);
    // == 鸡舍每日图片
    app.get('/images/days',LoginController.signinRequire,ImagesController.daysPage);
    // == 720加轮播图片
    app.post('/images/upload/720',ImagesController.uploadImage,ImagesController.overallImages);
    // == 720删除轮播图片
    app.post('/images/delete/720',ImagesController.deleteImages);
    // == 鸡舍每日图片
    app.get('/images/days/all/:id',LoginController.signinRequire,ImagesController.allPage);
    // == 鸡舍室内室外图片
    app.get('/images/days/side/:id',LoginController.signinRequire,ImagesController.sidePage);
    // == 评论管理
    app.get('/comment/:page',LoginController.signinRequire,AboutController.commentPage);
    // == 删除某条评论
    app.delete('/comment/list',AboutController.commentDelete);

    /****************************************个人信息*****************************************/
    // == 我的信息
    app.get('/my/view',LoginController.signinRequire,MyController.myInfo);
    // == 修改我的信息
    app.post('/my/update',LoginController.signinRequire,MyController.updateMyInfo);
    // == 修改密码页面
    app.get('/my/password',LoginController.signinRequire,MyController.myPassword);
    // == 修改密码
    app.post('/my/update/password',LoginController.signinRequire,MyController.updatePassword);

    /******************************************统计图表*****************************************/
    // == 农场小鸡总数、产蛋量、粮水统计
    app.post('/chart/chicken/farm',ChartController.chickenChart);
    // == 农场粮水统计图
    app.post('/chart/farm/food',ChartController.farmFood);
    // == 农场产蛋量统计图
    app.post('/chart/farm/egg',ChartController.farmEgg);
    // == 农场小鸡买入统计图
    app.post('/chart/farm/buy',ChartController.farmBuy);
    // == 农场小鸡卖出统计图
    app.post('/chart/farm/sell',ChartController.farmSell);
    // == 农场小鸡死亡统计图
    app.post('/chart/farm/die',ChartController.farmDie);
    // == 鸡舍温度统计图
    app.post('/chart/chicken/temperature',ChartController.temperatureChart);
    // == 鸡舍湿度统计图
    app.post('/chart/chicken/humidity',ChartController.humidityChart);
    // == 鸡舍产蛋统计图
    app.post('/chart/chicken/egg',ChartController.eggChart);
    // == 鸡舍粮水统计图
    app.post('/chart/chicken/food',ChartController.foodChart);
    // == 小鸡数量
    app.post('/chart/chicken/count',ChartController.countChicken);
    // == 蛋料比统计图
    app.post('/chart/farm/eggfood',ChartController.eggFoodChart);
    // == 死淘率统计图
    app.post('/chart/farm/dieout',ChartController.dieOutChart);
    // == 温度统计图
    app.post('/chart/farm/temhum',ChartController.temHumChart);

    // == 展示
    app.get('/exhibition/list/:page/',LoginController.signinRequire,ExhibitionController.listItem);
    // == 添加展示页面
    app.get('/exhibition/add',ExhibitionController.addPage);
    // == 添加展示
    app.post('/exhibition/add',ImagesController.uploadImage,ExhibitionController.uploadVideo,ExhibitionController.addItem);
    // == 删除展示信息
    app.delete('/exhibition/list',ExhibitionController.deleteItem);
    // == 编辑展示页面
    app.get('/exhibition/edit/:id',ExhibitionController.editPage);
    // == 修改展示信息
    app.post('/exhibition/update',ImagesController.uploadImage,ExhibitionController.uploadVideo,ExhibitionController.updateItem);

    // == 品牌简介
    app.get('/brand',BrandController.addPage);
    app.post('/brand/add',ImagesController.uploadImage,BrandController.uploadVideo,BrandController.addItem);

    // == 饲料信息
    app.get('/feed',FeedController.addPage);
    app.post('/feed/add',FeedController.addItem);


    // == 环境情况
    app.get('/environment/list/:page/',LoginController.signinRequire,EnvironmentController.listItem);
    // == 添加环境情况页面
    app.get('/environment/add',EnvironmentController.addPage);
    // == 添加环境情况
    app.post('/environment/add',EnvironmentController.addItem);
    // == 删除环境情况信息
    app.delete('/environment/list',EnvironmentController.deleteItem);
    // == 编辑环境情况页面
    app.get('/environment/edit/:id',EnvironmentController.editPage);
    // == 修改环境情况信息
    app.post('/environment/update',EnvironmentController.updateItem);

    // == 检测报告
    app.get('/check',ImagesController.checkPage);
    // == 检测报告文件上传
    app.post('/images/upload/check',ImagesController.uploadImage,ImagesController.checkImages);
    // == 清除检测报告
    app.post('/check/clear',ImagesController.checkClear);

    // == 履历下载
    app.get('/recode',RecodeController.downRecodePage);
    // == 履历批量生成
    app.post('/recode/down',RecodeController.downRecode);



    // == 404
    app.use(function (req,res) {
        res.type('text/html');
        res.status(404);
        res.render('./others/404',{
            title:"404"
        });
    });

    // == 500
    app.use(function (err,req,res,next) {
        console.error(err.stack);
        res.type('text/plain');
        res.status(500);
        res.send('500 - Server Error');
        next();
    });
};