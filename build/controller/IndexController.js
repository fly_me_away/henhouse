var API = require('../config/api');
var UtilService = require('../service/UtilService');
// == 首页
exports.indexPage = function (req,res) {
    res.render('index',{
        title:"慧养鸡管理平台"
    });
};

exports.messagesInfo = function (req,res) {
    var date = req.body.date;
    var method = 'get.messageByfarm.info';
    var field = JSON.stringify({loginId:req.session.user.id,date:date});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.other.messages,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                return res.json(data);
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                return res.json(data);
            }
        });
    });
};