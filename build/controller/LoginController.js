var api = require('../config/api');
var UtilService = require('../service/UtilService');
// == 后台登录页
exports.loginPage = function (req,res) {
    res.render('login',{
        pageId:"login",
        title:"慧养鸡管理平台"
    })
};

// == 点击登录按钮
exports.login = function (req,res) {
    var _login = req.body.login;
    var field = JSON.stringify(_login);
    var method = 'get.login.data';

    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,api.api.login,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                req.session.user = data.invoke_data;
                return res.json(data);
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                delete req.session.user;
                return res.json(data);
            }
        });
    });
};

// == 注销登录
exports.logout = function (req,res) {
    delete req.session.user;
    res.redirect('/login');
};

//需要用户登录
exports.signinRequire = function(req,res,next){
    var user = req.session.user;
    if(!user){
        return res.redirect('/login');
    }else {
        res.locals.user = user;
    }
    next();
};

