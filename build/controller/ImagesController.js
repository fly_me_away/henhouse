var Api = require('../config/api');
var UtilService = require('../service/UtilService');
var fs = require('fs');
var path = require('path');

// == 720
exports.overallPage = function (req,res) {
    var field = JSON.stringify({loginId:req.session.user.id});
    var method = 'find.carousel.opration';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,Api.api.images.bannerView,function (data) {
            res.render('./images/720',{
                title:"轮播图",
                data:data.invoke_data
            });
        });
    });
};

// == 全局图片
exports.globalPage = function (req,res) {
    res.render('./images/global',{
        title:"全局图片"
    });
};

// == 鸡舍每日图片
exports.daysPage = function (req,res) {
    var field = JSON.stringify({loginId:req.session.user.id});
    var method = 'get.hencoopList.data';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,Api.api.images.hencoopList,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                res.render('./images/days',{
                    title:"鸡舍每日图片",
                    data:data.hencoop_list
                });
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                res.redirect('/404');
            }
        });
    });
};

// == 图片
exports.imagePage = function (req,res) {
    res.render('./images/image',{
        title:"图片"
    });
};

// == 鸡舍室内室外图片
exports.sidePage = function (req,res) {
    var hencoop_id = req.params.id;
    var field = JSON.stringify({loginId:req.session.user.id,hencoop_id:hencoop_id});
    var method = 'get.hencoopCameras.data';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,Api.api.images.hencoopCameras,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                res.render('./images/sides',{
                    title:"鸡舍室内室外图片",
                    data:data.camera_list
                });
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                res.redirect('/404');
            }
        });
    });
};

// == 鸡舍每日图片全部
exports.allPage = function (req,res) {
    var camera_id = req.params.id;
    var field = JSON.stringify({loginId:req.session.user.id,camera_id:camera_id});
    var method = 'get.cameraImages.data';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,Api.api.images.cameraImages,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                res.render('./images/all',{
                    title:"全部",
                    data:data.camera_capture
                });
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                res.redirect('/404');
            }
        });
    });
};


// == 图片上传
exports.uploadImage = function (req,res,next) {
    var uploadImage = req.files.bannerfile;
    var filePath = uploadImage.path;
    var originalFileName = uploadImage.originalFilename;
    // 判断是否传入图片
    if (originalFileName){
        var farmImagePath = path.join(__dirname,'../../','public/src/update/','farmerid'+req.session.user.id+'/');
        fs.exists(farmImagePath,function (exists) {
            if(exists){
                fs.readFile(filePath,function (err,data) {
                    var timestamp = Date.now();
                    var type = uploadImage.type.split('/')[1];
                    var image = timestamp+'.'+type;
                    var newPath = farmImagePath+image;
                    fs.writeFile(newPath,data,function (err) {
                        req.imagePath = '/update/farmerid'+req.session.user.id+'/'+image;
                        next();
                    });
                });
            }else {
                fs.mkdir(farmImagePath,function (err) {
                    if(err)
                        throw err;
                    fs.readFile(filePath,function (err,data) {
                        var timestamp = Date.now();
                        var type = uploadImage.type.split('/')[1];
                        var image = timestamp+'.'+type;
                        var newPath = farmImagePath+image;

                        fs.writeFile(newPath,data,function (err) {
                            req.imagePath = '/update/farmerid'+req.session.user.id+'/'+image;
                            next();
                        });
                    });
                });
            }
        });
    }else{
        next();
    }
};

// == 轮播图片
exports.overallImages = function (req,res) {
    var url = req.body.url;
    var remarks = req.body.remarks;
    if (req.imagePath){
        var imagePath = 'http://'+Api.url.hostname+':3000'+req.imagePath;
        var field = JSON.stringify({loginId:req.session.user.id,imgURL:imagePath,remarks:remarks,url:url});
        var method = 'save.carouselSave.opration';
        UtilService.getSign(method,function (sign) {
            UtilService.getDataApi(method,field,sign,Api.api.images.banner,function (data) {
                res.redirect('/images/720');
            });
        });
    }
};

// == 删除轮播图
exports.deleteImages = function (req,res) {
    var farmCarouselId = req.body.farmCarouselId;
    var field = JSON.stringify({loginId:req.session.user.id,farmCarouselId:farmCarouselId});
    var method = 'find.deleteCarouselById.opration';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,Api.api.images.banner,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                return res.json(data);
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                return res.json(data);
            }
        });
    });
};

// == 检测报告
exports.checkPage = function (req,res) {
    var field = JSON.stringify({loginId:req.session.user.id,id:1});
    var method = 'get.editTesting.data';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,Api.api.images.checkShow,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                res.render('./others/check',{
                    title:"检测报告",
                    testing:data.testing
                });
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                return res.json(data);
            }
        });
    });
};

// == 检测报告图片上传
exports.checkImages = function (req,res) {
    var testingtime = req.body.testingtime;
    if (req.imagePath) {
        var imagePath = 'http://' + Api.url.hostname + ':3000' + req.imagePath;
    }else{
        var imagePath = '';
    }
    var field = JSON.stringify({loginId:req.session.user.id,testingimg:imagePath,testingtime:testingtime});
    var method = 'get.updateTesting.data';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,Api.api.images.check,function (data) {
            res.redirect('/check');
        });
    });
};

exports.checkClear = function (req,res) {
    var id = req.body.id;
    var method = 'get.clearTesting.data';
    var field = JSON.stringify({loginId:req.session.user.id,id:id});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,Api.api.other.clearCheck,function (data) {
            return res.json(data);
        });
    });
};
