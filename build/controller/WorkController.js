var API = require('../config/api');
var UtilService = require('../service/UtilService');
// == 工作日程
exports.schedulePage = function (req,res) {
    var page = req.params.page || 1;
    var method = 'get.breederWork.list';
    var field = JSON.stringify({loginId:req.session.user.id,pageNumber:page,pageSize:7});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.work.list,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                res.render('./work/schedule',{
                    title:"工作日程",
                    page:data.page,
                    data:data.invoke_data.works,
                    tasks:data.invoke_data.tasks
                });
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                res.redirect('/404');
            }
        });
    });
};
// == 添加工作日程
exports.addSchedule = function (req,res) {
    var _add = req.body.add;
    var method = 'save.breederWork.operation';
    var workItems = "["+UtilService.arrToJson(_add.scheduleContent,_add.workTime)+"]";
    var field = JSON.stringify({loginId:req.session.user.id,workItems:workItems,name:_add.scheduleName});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.work.add,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                res.redirect('/work/schedule/1/');
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                res.redirect('/404');
            }
        });
    });
};

// == 编辑工作日程
exports.editSchedule = function (req,res) {
    var workId = req.body.workId;
    var method = 'edit.breederWork.operation';
    var field = JSON.stringify({loginId:req.session.user.id,workId:workId});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.work.edit,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                return res.json(data);
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                return res.json(data);
            }
        });
    });
};

// == 更新工作日程
exports.updateSchedule = function (req,res) {
    var workId = req.body.workId;
    var _update = req.body.update;
    var workItems = "["+UtilService.arrToJson(_update.scheduleContent,_update.workTime)+"]";
    var field = JSON.stringify({loginId:req.session.user.id,workId:workId,workItems:workItems,name:_update.scheduleName});
    var method = 'update.breederWork.operation';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.work.update,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                res.redirect('/work/schedule/1/');
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                res.redirect('/404');
            }
        });
    });
};

// == 分配工作日程
exports.distributePage = function (req,res) {
    var page = req.params.page || 1;
    var breederId = req.query.breederId || '';
    var status = req.query.status || '';
    var hencoopNumber = req.query.hencoopNumber || '';
    var dayAge = req.query.dayAge || '';
    var varieties = req.query.varieties || '';
    var workId = req.query.workid || '';
    var method = 'get.chicken.list';
    var field = JSON.stringify({loginId:req.session.user.id,pageNumber:page,pageSize:6,workId:workId,breederId:breederId,dayAge:dayAge,varieties:varieties,status:status,hencoopNumber:hencoopNumber});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.henhouse.list,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                res.render('./work/distribution',{
                    title:"分配工作日程",
                    page:data.page,
                    data:data.invoke_data,
                    breeders:data.invoke_data_breeders,
                    varieties:data.invoke_data_varieties,
                    henhouseIds:data.invoke_data_hasWorkHencoopsIds
                });
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                res.redirect('/404');
            }
        });
    });
};

// == 确认分配工作日程
exports.confirmDistribution = function (req,res) {
    var workId = req.body.workId;
    var hencoopeIds = req.body.hencoopeIds;
    var field = JSON.stringify({loginId:req.session.user.id,workId:workId,hencoopeIds:hencoopeIds});
    var method = 'distribution.breederWork.operation';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.work.distribute,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                return res.json(data);
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                return res.json(data);
            }
        });
    });
};

// == 删除工作日程
exports.deleteSchedule = function (req,res) {
    var workId = req.body.workId;
    var field = JSON.stringify({loginId:req.session.user.id,workId:workId});
    var method = 'delete.breederWork.operation';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.work.delete,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                return res.json(data);
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                return res.json(data);
            }
        });
    });
};

// == 工作记录
// exports.recodePage = function (req,res) {
//     var page = req.params.page || 1;
//     var method = 'get.worklog.list';
//     var field = JSON.stringify({loginId:req.session.user.id,pageNumber:page,pageSize:7});
//     UtilService.getSign(method,function (sign) {
//         UtilService.getDataApi(method,field,sign,API.api.work.recode,function (data) {
//             if (data.invoke_result == "INVOKE_SUCCESS"){
//                 res.render('./work/recode',{
//                     title:"工作记录",
//                     page:data.page,
//                     data:data.invoke_data,
//                     tasks:data.invoke_data.tasks
//                 });
//             }else if(data.invoke_result == "INVOKE_FAILURE"){
//                 res.redirect('/404');
//             }
//         });
//     });
// };

// == 工作记录
exports.recodePage = function (req,res) {
    var breederId = req.query.breederId || '';
    var hencoopNumber = req.query.hencoopNumber || '';
    var startDate = req.query.startDate || '';
    var endDate = req.query.endDate || '';
    var page = req.params.page || 1;
    var method = 'get.hencoopworkitmes.data';
    var field = JSON.stringify({loginId:req.session.user.id,hencoop_number:hencoopNumber,ch_breeder_id:breederId,start_date:startDate,end_date:endDate,pageNumber:page,pageSize:7});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.work.detail,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                res.render('./work/recode',{
                    title:"工作记录",
                    page:data.page,
                    data:data.invoke_data,
                    breeders:data.breeder_list,
                    hencoopNumbers:data.hencoop_list
                });
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                res.redirect('/404');
            }
        });
    });
};

// == 工作记录导出Excel
exports.exportExcel = function (req,res) {
    var breederId = req.body.breederId || '';
    var hencoopNumber = req.body.hencoopNumber || '';
    var startDate = req.body.startDate || '';
    var endDate = req.body.endDate || '';
    var method = 'get.hencoopworkitmesexecle.data';
    var field = JSON.stringify({loginId:req.session.user.id,hencoop_number:hencoopNumber,ch_breeder_id:breederId,start_date:startDate,end_date:endDate});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.work.excel,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                return res.json(data);
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                return res.json(data);
            }
        });
    });
};

// == 工作任务
exports.tasksPage = function (req,res) {
    var page = req.params.page || 1;
    var method = 'get.breedertemporarytasks.data';
    var field = JSON.stringify({loginId:req.session.user.id,pageNumber:page,pageSize:7});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.work.listTask,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                res.render('./work/tasks',{
                    title:"工作任务",
                    page:data.page,
                    data:data.invoke_data,
                    breeders:data.breeder_list
                });
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                res.redirect('/404');
            }
        });
    });
};

// == 添加工作任务
exports.addTask = function (req,res) {
    var title = req.body.title;
    var content = req.body.content;
    var field = JSON.stringify({loginId:req.session.user.id,title:title,content:content});
    var method = 'get.savebreedertemporarytask.data';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.work.addTask,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                return res.json(data);
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                return res.json(data);
            }
        });
    });
};

// == 编辑工作任务
exports.editTask = function (req,res) {
    var id = req.body.id;
    var title = req.body.title;
    var content = req.body.content;
    var field = JSON.stringify({loginId:req.session.user.id,id:id,title:title,content:content});
    var method = 'get.updatebreedertemporarytask.data';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.work.editTask,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                return res.json(data);
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                return res.json(data);
            }
        });
    });
};

// == 删除工作任务
exports.deleteTask = function (req,res) {
    var id = req.body.id;
    var field = JSON.stringify({loginId:req.session.user.id,id:id});
    var method = 'get.deletebreedertemporarytask.data';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.work.deleteTask,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                return res.json(data);
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                return res.json(data);
            }
        });
    });
};

// == 发送工作任务
exports.sendTask = function (req,res) {
    var id = req.body.id;
    var breederId = req.body.breederId;
    var field = JSON.stringify({loginId:req.session.user.id,id:id,ch_breeder_id:breederId});
    var method = 'get.sendbreedertemporarytask.data';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.work.sendTask,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                return res.json(data);
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                return res.json(data);
            }
        });
    });
};


