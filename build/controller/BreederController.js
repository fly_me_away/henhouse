var api = require('../config/api');
var UtilService = require('../service/UtilService');
// == 饲养员列表
exports.listPage = function (req,res) {
    // res.render('./breeder/list',{
    //     title:"饲养员列表",
    //     notes:"注：....."
    // });
    var page = req.params.page || 1;
    var method = 'get.breeder.list';
    var field = JSON.stringify({loginId:req.session.user.id,pageNumber:page,pageSize:7});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,api.api.breeder.list,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.render('./breeder/list',{
                    title:"饲养员列表",
                    notes:"注：.....",
                    data:data.invoke_data.list,
                    page:data.invoke_data.page
                });
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};

// == 饲养员分页
// exports.listPagination = function (req,res) {
//     var page = req.body.page;
//     var method = 'get.breeder.list';
//     var field = JSON.stringify({loginId:req.session.user.id,pageNumber:page,pageSize:7});
//     UtilService.getSign(method,function (sign) {
//         UtilService.getDataApi(method,field,sign,api.api.breeder.list,function (data) {
//             if (data.invoke_result == 'INVOKE_SUCCESS'){
//                 var list = data.invoke_data;
//                 return res.json(list);
//             }else if(data.invoke_result == 'INVOKE_FAILURE'){
//                 return res.json(data);
//             }
//         });
//     });
// };

// == 添加饲养员
exports.addBreeder = function (req,res) {
    var field = JSON.stringify({loginId:req.session.user.id});
    var method = 'add.breeder.info';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,api.api.breeder.add,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                return res.json(data);
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};

// == 删除饲养员
exports.deleteBreeder = function (req,res) {
    var breederid = req.body.breederId;
    var field = JSON.stringify({loginId:req.session.user.id,breederId:breederid});
    var method = 'delete.breeder.operation';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,api.api.breeder.delete,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                return res.json(data);
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};

// == 保存饲养员
exports.saveBreeder = function (req,res) {
    var _save = req.body.save;
        _save.loginId = req.session.user.id;
        _save.type = 2;
    if (req.imagePath){
        _save.head_img_url = 'http://' + api.url.hostname + ':3000' + req.imagePath;
    }else{
        _save.head_img_url = "";
    }
    var field = JSON.stringify(_save);
    var method = 'save.breeder.info';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,api.api.breeder.save,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.redirect('/breeder/list/1')
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};

// == 查看饲养员
exports.viewBreeder = function (req,res) {
    var breederid = req.params.id;
    var field = JSON.stringify({loginId:req.session.user.id,breederId:breederid});
    var method = 'get.breeder.info';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,api.api.breeder.delete,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                console.log(data);
                res.render('./breeder/view',{
                    title:"查看饲养员",
                    notes:"注：.....",
                    data:data.invoke_data,
                    hencoops:data.invoke_data_hencoops
                });
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};

// == 饲养员表单验证
exports.formValidity = function (req,res) {
    var username = req.body.username;
    var method = 'check.breeder.username';
    var field = JSON.stringify({userName:username});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,api.api.breeder.form.username,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                return res.json(data);
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};
