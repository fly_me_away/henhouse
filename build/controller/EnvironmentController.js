var API = require('../config/api');
var UtilService = require('../service/UtilService');

// == 环境情况列表
exports.listItem = function (req,res) {
    var page = req.params.page || 1;
    var method = 'get.environmentList.data';
    var field = JSON.stringify({loginId:req.session.user.id,pageNumber:page,pageSize:7});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.environment.list,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.render('./environment/list',{
                    title:"环境情况",
                    page:data.invoke_data.page,
                    data:data.invoke_data.list,
                    hencoop:data.invoke_data.hencooplist
                });
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};

// == 环境添加页
exports.addPage = function (req,res) {
    var page = req.params.page || 1;
    var method = 'get.environmentList.data';
    var field = JSON.stringify({loginId:req.session.user.id,pageNumber:page,pageSize:7});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.environment.list,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.render('./environment/add',{
                    title:"添加环境信息",
                    hencoop:data.invoke_data.hencooplist
                });
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};

// == 添加环境信息
exports.addItem = function (req,res) {
    var add = req.body.add;
    var method = 'get.addEnvironment.data';
    var field = JSON.stringify({loginId:req.session.user.id,hencoop_id:add.hencoop_id,title:add.environment_title,content:add.environment_content});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.environment.add,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.redirect('/environment/list/1')
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};

// == 删除展示信息
exports.deleteItem = function (req,res) {
    var id = req.body.id;
    var method = 'get.deleteEnvironment.data';
    var field = JSON.stringify({loginId:req.session.user.id,id:id});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.environment.del,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                return res.json(data);
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};

// == 编辑展示信息
exports.editPage = function (req,res) {
    var id = req.params.id;
    var method = 'get.edit.data';
    var field = JSON.stringify({loginId:req.session.user.id,id:id});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.environment.editShow,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.render('./environment/edit',{
                    title:"编辑环境信息信息",
                    exhid:data.environmental_hencoop.id,
                    exhtitle:data.environmental_hencoop.title,
                    exhcontent:data.environmental_hencoop.content,
                    hencoop:data.hencooplist,
                });
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};

// == 修改展示信息
exports.updateItem = function (req,res) {
    var edit = req.body.edit;
    var method = 'get.updateEnvironment.data';
    var field = JSON.stringify({loginId:req.session.user.id,id:edit.environment_id,hencoop_id:edit.hencoop_id,title:edit.environment_title,content:edit.environment_content});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.environment.update,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.redirect('/environment/list/1')
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};