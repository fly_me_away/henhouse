var api = require('../config/api');
var UtilService = require('../service/UtilService');
// == 我的信息
exports.myInfo = function (req,res) {
    res.render('./my/view',{
        title:"我的信息"
    });
};

// == 修改我的信息
exports.updateMyInfo = function (req,res) {
    var _update = req.body.update;
    _update.loginId = req.session.user.id;
    var field = JSON.stringify(_update);
    var method = 'update.farmerInfo.data';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,api.api.my.update,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                return res.json(data);
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                return res.json(data);
            }
        });
    });
};
// == 修改密码页面
exports.myPassword = function (req,res) {
    res.render('./my/password',{
        title:"修改密码"
    });
};
// == 修改密码
exports.updatePassword = function (req,res) {
    var _update = req.body.update;
    _update.loginId = req.session.user.id;
    var field = JSON.stringify(_update);
    var method = 'get.modifyFarmerPassword.data';
    console.log(field);
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,api.api.my.password,function (data) {
            console.log(data);
            if (data.invoke_result == "INVOKE_SUCCESS"){
                res.redirect('/logout');
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                res.render('./my/password',{
                    title:"password",
                    message:"修改密码",
                    alert:data.invoke_message
                });
            }
        });
    });
};