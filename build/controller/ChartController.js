var API = require('../config/api');
var UtilService = require('../service/UtilService');

// == 农场小鸡统计图
exports.chickenChart = function (req,res) {
    var field = JSON.stringify({loginId:req.session.user.id});
    var method = 'get.check.data';
    UtilService.getDataApi(method,field,"",API.api.chart.chickenCount,function (data) {
        if (data.invoke_result == "INVOKE_SUCCESS"){
            return res.json(data);
        }else if(data.invoke_result == "INVOKE_FAILURE"){
            return res.json(data);
        }
    });
};

// == 农场粮水统计
exports.farmFood = function (req,res) {
    var chooseTimeMethod = req.body.chooseTimeMethod;
    var field = JSON.stringify({loginId:req.session.user.id,chooseTimeMethod:chooseTimeMethod});
    var method = 'get.foodandwaterbyfarm.info';
    UtilService.getDataApi(method,field,"",API.api.chart.farmFood,function (data) {
        if (data.invoke_result == "INVOKE_SUCCESS"){
            return res.json(data);
        }else if(data.invoke_result == "INVOKE_FAILURE"){
            return res.json(data);
        }
    });
};

// == 农场产蛋量统计
exports.farmEgg = function (req,res) {
    var chooseTimeMethod = req.body.chooseTimeMethod;
    var field = JSON.stringify({loginId:req.session.user.id,chooseTimeMethod:chooseTimeMethod});
    var method = 'get.eggNumberByfarm.info';
    UtilService.getDataApi(method,field,"",API.api.chart.farmEgg,function (data) {
        if (data.invoke_result == "INVOKE_SUCCESS"){
            return res.json(data);
        }else if(data.invoke_result == "INVOKE_FAILURE"){
            return res.json(data);
        }
    });
};

// == 农场小鸡买入统计
exports.farmBuy = function (req,res) {
    var chooseTimeMethod = req.body.chooseTimeMethod;
    var field = JSON.stringify({loginId:req.session.user.id,chooseTimeMethod:chooseTimeMethod});
    var method = 'get.buyChickenNum.operation';
    UtilService.getDataApi(method,field,"",API.api.chart.farmBuy,function (data) {
        if (data.invoke_result == "INVOKE_SUCCESS"){
            return res.json(data);
        }else if(data.invoke_result == "INVOKE_FAILURE"){
            return res.json(data);
        }
    });
};

// == 农场小鸡卖出统计
exports.farmSell = function (req,res) {
    var chooseTimeMethod = req.body.chooseTimeMethod;
    var field = JSON.stringify({loginId:req.session.user.id,chooseTimeMethod:chooseTimeMethod});
    var method = 'get.sellChickenNum.operation';
    UtilService.getDataApi(method,field,"",API.api.chart.farmSell,function (data) {
        if (data.invoke_result == "INVOKE_SUCCESS"){
            return res.json(data);
        }else if(data.invoke_result == "INVOKE_FAILURE"){
            return res.json(data);
        }
    });
};

// == 农场小鸡死亡统计
exports.farmDie = function (req,res) {
    var chooseTimeMethod = req.body.chooseTimeMethod;
    var field = JSON.stringify({loginId:req.session.user.id,chooseTimeMethod:chooseTimeMethod});
    var method = 'get.deathChickenNum.operation';
    UtilService.getDataApi(method,field,"",API.api.chart.farmDie,function (data) {
        if (data.invoke_result == "INVOKE_SUCCESS"){
            return res.json(data);
        }else if(data.invoke_result == "INVOKE_FAILURE"){
            return res.json(data);
        }
    });
};

// == 鸡舍温度统计图
exports.temperatureChart = function (req,res) {
    var henhouseId = req.body.henhouseId;
    var field = JSON.stringify({loginId:req.session.user.id,houseId:henhouseId});
    var method = 'get.temperature.info';
    UtilService.getDataApi(method,field,"",API.api.chart.temperature,function (data) {
        if (data.invoke_result == "INVOKE_SUCCESS"){
            return res.json(data);
        }else if(data.invoke_result == "INVOKE_FAILURE"){
            return res.json(data);
        }
    });
};

// == 鸡舍湿度统计图
exports.humidityChart = function (req,res) {
    var henhouseId = req.body.henhouseId;
    var field = JSON.stringify({loginId:req.session.user.id,houseId:henhouseId});
    var method = 'get.humidity.info';
    UtilService.getDataApi(method,field,"",API.api.chart.humidity,function (data) {
        if (data.invoke_result == "INVOKE_SUCCESS"){
            return res.json(data);
        }else if(data.invoke_result == "INVOKE_FAILURE"){
            return res.json(data);
        }
    });
};

// == 鸡舍产蛋量统计图
exports.eggChart = function (req,res) {
    var henhouseId = req.body.henhouseId;
    var chooseTimeMethod = req.body.chooseTimeMethod;
    var field = JSON.stringify({loginId:req.session.user.id,houseId:henhouseId,chooseTimeMethod:chooseTimeMethod});
    var method = 'get.eggNumber.info';
    UtilService.getDataApi(method,field,"",API.api.chart.egg,function (data) {
        if (data.invoke_result == "INVOKE_SUCCESS"){
            return res.json(data);
        }else if(data.invoke_result == "INVOKE_FAILURE"){
            return res.json(data);
        }
    });
};

// == 鸡舍粮水统计图
exports.foodChart = function (req,res) {
    var henhouseId = req.body.henhouseId;
    var chooseTimeMethod = req.body.chooseTimeMethod;
    var field = JSON.stringify({loginId:req.session.user.id,houseId:henhouseId,chooseTimeMethod:chooseTimeMethod});
    var method = 'get.foodandwater.info';
    UtilService.getDataApi(method,field,"",API.api.chart.foodAndWater,function (data) {
        if (data.invoke_result == "INVOKE_SUCCESS"){
            return res.json(data);
        }else if(data.invoke_result == "INVOKE_FAILURE"){
            return res.json(data);
        }
    });
};

// == 小鸡数量
exports.countChicken = function (req,res) {
    var chickenVarietiesId = req.body.chickenVarietiesId;
    var breederId = req.body.breederId;
    var age = req.body.age;
    var gender = req.body.gender;
    var field = JSON.stringify({loginId:req.session.user.id,chickenVarietiesId:chickenVarietiesId,breederId: breederId,age: age,gender:gender});
    var method = 'get.chickenNumberByfarm.info';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.chart.chickenNum,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                return res.json(data)
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                return res.json(data)
            }
        });
    });
};

// == 蛋料比统计图
exports.eggFoodChart = function (req,res) {
    var year = req.body.year || new Date().getFullYear();
    var month = req.body.month || new Date().getMonth()+1;
    var field = JSON.stringify({loginId:req.session.user.id,year:year,month:month});
    var method = 'get.egg_feed_column.data';
    UtilService.getDataApi(method,field,"",API.api.chart.eggFood,function (data) {
        if (data.invoke_result == "INVOKE_SUCCESS"){
            return res.json(data);
        }else if(data.invoke_result == "INVOKE_FAILURE"){
            return res.json(data);
        }
    });
};

// == 死淘率统计图
exports.dieOutChart = function (req,res) {
    var year = req.body.year || new Date().getFullYear();
    var month = req.body.month || new Date().getMonth()+1;
    var field = JSON.stringify({loginId:req.session.user.id,year:year,month:month});
    var method = 'get.death_column.data';
    UtilService.getDataApi(method,field,"",API.api.chart.dieOut,function (data) {
        if (data.invoke_result == "INVOKE_SUCCESS"){
            return res.json(data);
        }else if(data.invoke_result == "INVOKE_FAILURE"){
            return res.json(data);
        }
    });
};

// == 温度统计图
exports.temHumChart = function (req,res) {
    var year = req.body.year || new Date().getFullYear();
    var month = req.body.month || new Date().getMonth()+1;
    var field = JSON.stringify({loginId:req.session.user.id,year:year,month:month});
    var method = 'get.temperature.data';
    UtilService.getDataApi(method,field,"",API.api.chart.temHum,function (data) {
        if (data.invoke_result == "INVOKE_SUCCESS"){
            return res.json(data);
        }else if(data.invoke_result == "INVOKE_FAILURE"){
            return res.json(data);
        }
    });
};

