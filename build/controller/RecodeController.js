var api = require('../config/api');
var UtilService = require('../service/UtilService');
// == 履历下载
exports.downRecodePage = function (req,res) {
    res.render('./others/downRecode',{
        title:"履历下载"
    });
};

exports.downRecode = function (req,res) {
    var count = req.body.count;
    var method = 'get.batchGenerate.data';
    var field = JSON.stringify({loginId:req.session.user.id,count:count});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,api.api.other.downRecode,function (data) {
            return res.json(data);
        });
    });
};