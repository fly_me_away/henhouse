var API = require('../config/api');
var UtilService = require('../service/UtilService');
// == 鸡舍列表
exports.listPage = function (req,res) {
    var page = req.params.page || 1;
    var breederId = req.query.breederId || '';
    var status = req.query.status || '';
    var hencoopNumber = req.query.hencoopNumber || '';
    var method = 'get.chicken.list';
    var field = JSON.stringify({loginId:req.session.user.id,pageNumber:page,pageSize:6,breederId:breederId,status:status,hencoopNumber:hencoopNumber});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.henhouse.list,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.render('./henhouse/list',{
                    title:"鸡舍列表",
                    page:data.page,
                    data:data.invoke_data,
                    breeders:data.invoke_data_breeders,
                    hencoopNumbers:data.invoke_data_hencoopNumbers
                });
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};

// == 添加鸡舍
exports.addHenhouse = function (req,res) {
    var _add = req.body.add;
        _add.loginId = req.session.user.id;
    var field = JSON.stringify(_add);
    var method = 'save.chicken.operation';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.henhouse.add,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.redirect('/henhouse/list/1/');
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};

// == 查看鸡舍简介
exports.introHenhouse = function (req,res) {
    var henhouseId = req.body.henhouseId;
    var field = JSON.stringify({loginId:req.session.user.id,huoseId:henhouseId});
    var method = 'get.chicken.synopsis';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.henhouse.intro,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                return res.json(data);
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};

// == 查看鸡舍详情
exports.detailHenhouse = function (req,res) {
    var hencoopId = req.query.henhouseid;
    var field = JSON.stringify({loginId:req.session.user.id,hencoopId:hencoopId});
    var method = 'get.chickenDetail.list';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.henhouse.detail,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.render('./henhouse/details',{
                    title:"鸡舍详情",
                    hencoopNumber:data.chickenNumberList.hencoop_number,
                    chickenDetailList:data.chickenDetailList,
                    liveAddress:data.liveAddresslist
                });
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};

// == 鸡舍统计
exports.statisticsPage = function (req,res) {
    var field = JSON.stringify({loginId:req.session.user.id});
    var method = 'get.breederList.list';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.other.breederList,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.render('./henhouse/statistics',{
                    title:"鸡舍统计",
                    breederList:data.breederList,
                    varietiesChicken:data.varietiesChicken
                });
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};

// == 编辑鸡舍获取信息
exports.editHenhouse = function (req,res) {
    var henhouseId = req.body.henhouseId;
    var field = JSON.stringify({loginId:req.session.user.id,id:henhouseId});
    var method = 'find.chicken.operation';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.henhouse.edit,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                return res.json(data);
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};

// == 更新鸡舍
exports.updateHenhouse = function (req,res) {
    var _update = req.body.update;
    _update.loginId = req.session.user.id;
    _update.id = req.body.henhouseId;
    var field = JSON.stringify(_update);
    var method = 'update.chicken.operation';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.henhouse.delete,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.redirect('/henhouse/list/1/');
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};

// == 删除鸡舍
exports.deleteHenhouse = function (req,res) {
    var henhouseId = req.body.henhouseId;
    var method = 'delect.chicken.operation';
    var field = JSON.stringify({loginId:req.session.user.id,huoseId:henhouseId});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.henhouse.update,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                return res.json(data);
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};

// == 鸡舍重要信息
exports.messagesHenhouse = function (req,res) {
    var henhouseId = req.body.henhouseId;
    var date = req.body.date;
    var method = 'get.importantMessage.operation';
    var field = JSON.stringify({loginId:req.session.user.id,huoseId:henhouseId,date:date});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.henhouse.message,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                return res.json(data);
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};

// == 鸡舍控制
exports.controlPage = function (req,res) {
    var page = req.params.page || 1;
    var method = 'get.varietiesInfo.data';
    var field = JSON.stringify({loginId:req.session.user.id,pageNumber:page,pageSize:7});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.varieties.list,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.render('./henhouse/control',{
                    title:"鸡舍控制",
                    page:data.invoke_data.page,
                    data:data.invoke_data.list
                });
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};

// == 鸡舍控制列表
exports.controlListPage = function (req,res) {
    var varietyId = req.query.varietyId;
    var method = 'get.control.list';
    var field = JSON.stringify({loginId:req.session.user.id,varietyId:varietyId});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.henhouse.controlList,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.render('./henhouse/controllist',{
                    title:"鸡舍控制列表",
                    data:data.invoke_data,
                    variety:data.invoke_data_varietiesName
                });
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};

// == 添加控制流程
exports.addControl = function (req,res) {
    var varietyId = req.query.varietyId;
    var method = 'add.control.operation';
    var field = JSON.stringify({loginId:req.session.user.id,varietyId:varietyId});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.henhouse.controlAdd,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.render('./henhouse/controladd',{
                    title:"添加鸡舍控制",
                    data:data.invoke_data,
                    variety:data.invoke_data_varietiesName,
                    controlTask:data.invoke_data_controlTasks
                });
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};

// == 保存鸡舍控制流程
exports.saveControl = function (req,res) {
    var _save = req.body.save;
        _save.loginId = req.session.user.id;
    var method = 'save.control.operation';
    var field = JSON.stringify(_save);
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.henhouse.controlSave,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                return res.json(data);
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};

// == 查看鸡舍控制流程
exports.viewControl = function (req,res) {
    var controlId = req.query.controlid;
    var varietyId = req.query.varietyId;
    var field = JSON.stringify({loginId:req.session.user.id,varietyId:varietyId,controlId:controlId});
    var method = 'edit.control.operation';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.henhouse.controlView,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.render('./henhouse/controlview',{
                    title:"查看控制流程",
                    variety:data.invoke_data_varietiesName,
                    month:data.invoke_data_control
                });
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};

// == 获取控制流程分页数据
exports.ajaxControl = function (req,res) {
    var controlId = req.body.controlId;
    var varietyId = req.body.varietyId;
    var field = JSON.stringify({loginId:req.session.user.id,varietyId:varietyId,controlId:controlId});
    var method = 'edit.control.operation';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.henhouse.controlView,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                return res.json(data);
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};

// == 编辑鸡舍控制流程
exports.editControl = function (req,res) {
    var controlId = req.query.controlid;
    var varietyId = req.query.varietyId;
    var field = JSON.stringify({loginId:req.session.user.id,varietyId:varietyId,controlId:controlId});
    var method = 'edit.control.operation';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.henhouse.controlView,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.render('./henhouse/controledit',{
                    title:"编辑控制流程",
                    variety:data.invoke_data_varietiesName,
                    month:data.invoke_data_control,
                    data:data.invoke_data
                });
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};

// == 更新鸡舍控制流程
exports.updateControl = function (req,res) {
    var _save = req.body.save;
    _save.loginId = req.session.user.id;
    var method = 'update.control.operation';
    var field = JSON.stringify(_save);
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.henhouse.controlUpdate,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                return res.json(data);
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};

// == 删除控制流程
exports.deleteControl = function (req,res) {
    var controlId = req.body.controlId;
    var field = JSON.stringify({loginId:req.session.user.id,controlId:controlId});
    var method = 'delete.control.operation';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.henhouse.controlDelete,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                return res.json(data);
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};

// == 添加鸡舍编号数据校验
exports.hencoopNumForm = function (req,res) {
    var hencoopNumber = req.body.hencoopNumber;
    var field = JSON.stringify({loginId:req.session.user.id,hencoopNumber:hencoopNumber});
    var method = 'get.addChickenDataCheck.list';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.henhouse.form.hencoopNum,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                return res.json(data);
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};

// == 添加鸡舍硬件ID数据校验
exports.hardwareIdForm = function (req,res) {
    var hardware = req.body.hardware;
    var field = JSON.stringify({loginId:req.session.user.id,hardware:hardware});
    var method = 'get.addChickenHardWareIdCheck.list';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.henhouse.form.hardware,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                return res.json(data);
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};

// 鸡舍绑定摄像头
exports.addCamera = function (req,res) {
    var name = req.body.name;
    var serialNum = req.body.serialNum;
    var verificationCode = req.body.verificationCode;
    var remarks = req.body.remarks;
    var hencoopId = req.body.hencoopId;
    var field = JSON.stringify({loginId:req.session.user.id,name:name,serialNum:serialNum,verificationCode:verificationCode,hencoopId:hencoopId,remarks:remarks});
    var method = 'get.addcamera.data';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.camera.addCamera,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                return res.json(data);
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};

// 鸡舍删除摄像头
exports.deleteCamera = function (req,res) {
    var cameraId = parseInt(req.body.cameraId);
    var field = JSON.stringify({loginId:req.session.user.id,cameraId:cameraId});
    var method = 'get.deleteCamera.data';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.camera.deleteCamera,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                return res.json(data);
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};

// 鸡舍查看摄像头
exports.viewCamera = function (req,res) {
    var cameraId = parseInt(req.body.videoid);
    var field = JSON.stringify({loginId:req.session.user.id,cameraId:cameraId});
    var method = 'get.seecamera.data';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.camera.viewCamera,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                return res.json(data);
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};

// 鸡舍更新摄像头
exports.updateCamera = function (req,res) {
    var cameraId = parseInt(req.body.cameraId);
    var name = req.body.name;
    var serialNum = req.body.serialNum;
    var verificationCode = req.body.verificationCode;
    var remarks = req.body.remarks;
    var hencoopId = parseInt(req.body.hencoopId);
    var field = JSON.stringify({loginId:req.session.user.id,name:name,serialNum:serialNum,VerificationCode:verificationCode,hencoopId:hencoopId,remarks:remarks,cameraId:cameraId});
    var method = 'get.updateCamera.data';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.camera.updateCamera,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                return res.json(data);
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};

// == 鸡舍导出名片
exports.exportCard = function (req,res) {
    var henhouseId = req.body.henhouseId;
    var field = JSON.stringify({loginId:req.session.user.id,hencoop_id:henhouseId});
    var method = 'get.export.data';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.henhouse.exportCard,function (data) {
            return res.json(data);
        });
    });
}