var API = require('../config/api');
var UtilService = require('../service/UtilService');
// == 关于我们
exports.aboutPage = function (req,res) {
    var method = 'get.aboutUs.list';
    var field = JSON.stringify({loginId:req.session.user.id});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.other.aboutView,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.render('./others/about',{
                    title:"关于我们",
                    data:data.invoke_data.aboutus
                });
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};

// == 关于我们提交数据
exports.setAboutContent = function (req,res) {
    var aboutus = req.body.aboutus;
    var method = 'get.aboutUsInfo.data';
    var field = JSON.stringify({farmId:req.session.user.id,aboutus:aboutus});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.other.about,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.redirect('/about');
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};

// == 意见反馈
exports.feedbackPage = function (req,res) {
    var page = req.params.page || 1;
    var method = 'get.feedbackInfo.data';
    var field = JSON.stringify({loginId:req.session.user.id,pageNumber:page,pageSize:6});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.other.feedback,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.render('./others/feedback',{
                    title:"意见反馈",
                    notes:"注：.....",
                    page:data.invoke_data.page,
                    data:data.invoke_data.list
                });
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};

// == 删除意见反馈
exports.feedbackDelete = function (req,res) {
    var feedbackid = req.body.feedbackid;
    var method = 'get.deleteFeedbackById.data';
    var field = JSON.stringify({feedbackId:feedbackid});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.other.feedbackDelete,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                return res.json(data);
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};

// == 评论管理
exports.commentPage = function (req,res) {
  var page = req.params.page || 1;
  var method = 'get.evaluateInfo.data';
  var field = JSON.stringify({loginId:req.session.user.id,pageNumber:page,pageSize:6});
  UtilService.getSign(method,function (sign) {
    UtilService.getDataApi(method,field,sign,API.api.other.feedback,function (data) {
      if (data.invoke_result == 'INVOKE_SUCCESS'){
        res.render('./others/comment',{
          title:"评论管理",
          notes:"注：.....",
          page:data.invoke_data.page,
          data:data.invoke_data.list
        });
      }else if(data.invoke_result == 'INVOKE_FAILURE'){
        res.redirect('/404');
      }
    });
  });
};

// == 删除意见反馈
exports.commentDelete = function (req,res) {
  var commentid = req.body.commentid;
  var method = 'get.deleteEvaluateById.data';
  var field = JSON.stringify({id:commentid});
  UtilService.getSign(method,function (sign) {
    UtilService.getDataApi(method,field,sign,API.api.other.feedback,function (data) {
      if (data.invoke_result == 'INVOKE_SUCCESS'){
        return res.json(data);
      }else if(data.invoke_result == 'INVOKE_FAILURE'){
        return res.json(data);
      }
    });
  });
};

