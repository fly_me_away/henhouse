var API = require('../config/api');
var fs = require('fs');
var path = require('path');
var UtilService = require('../service/UtilService');

// == 品牌简介添加页
exports.addPage = function (req,res) {
    var method = 'get.editBrand.data';
    var field = JSON.stringify({loginId:req.session.user.id,id:1});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.brand.view,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.render('./brand/add',{
                    title:"品牌简介",
                    brand:data.brand
                });
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};
// == 添加视频
exports.uploadVideo = function (req,res,next) {
    var uploadVideo = req.files.videofile;
    var filePath = uploadVideo.path;
    var originalFileName = uploadVideo.originalFilename;
    // 判断是否传入图片
    if (originalFileName){
        var farmImagePath = path.join(__dirname,'../../','public/src/update/','farmerid'+req.session.user.id+'/');
        fs.exists(farmImagePath,function (exists) {
            if(exists){
                fs.readFile(filePath,function (err,data) {
                    var timestamp = Date.now();
                    var type = uploadVideo.type.split('/')[1];
                    var image = timestamp+'.'+type;
                    var newPath = farmImagePath+image;
                    fs.writeFile(newPath,data,function (err) {
                        req.videoPath = '/update/farmerid'+req.session.user.id+'/'+image;
                        next();
                    });
                });
            }else {
                fs.mkdir(farmImagePath,function (err) {
                    if(err)
                        throw err;
                    fs.readFile(filePath,function (err,data) {
                        var timestamp = Date.now();
                        var type = uploadVideo.type.split('/')[1];
                        var image = timestamp+'.'+type;
                        var newPath = farmImagePath+image;

                        fs.writeFile(newPath,data,function (err) {
                            req.imagePath = '/update/farmerid'+req.session.user.id+'/'+image;
                            next();
                        });
                    });
                });
            }
        });
    }else{
        next();
    }
};
// == 添加品牌简介
exports.addItem = function (req,res) {
    if (req.imagePath) {
        var imagePath = 'http://' + API.url.hostname + ':3000' + req.imagePath;
    }else{
        var imagePath = req.body.oldposterfile || '';
    }
    if (req.videoPath) {
        var videoPath = 'http://' + API.url.hostname + ':3000' + req.videoPath;
    }else{
        var videoPath = req.body.oldvideofile || '';
    }
    var add = req.body.add;
    var method = 'get.updateBrand.data';
    var field = JSON.stringify({loginId:req.session.user.id,brand_name:add.brand_name,brand_content:add.brand_content,brand_video:videoPath,brand_poster:imagePath});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.brand.add,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.redirect('/brand')
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};
