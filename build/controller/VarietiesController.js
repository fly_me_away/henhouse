var api = require('../config/api');
var UtilService = require('../service/UtilService');
// == 品种信息列表列表
exports.listPage = function (req,res) {
    res.render('./varieties/list',{
        title:"品种信息列表"
    });
};

// == 品种信息分页分页
exports.listPagination = function (req,res) {
    var page = req.body.page;
    var method = 'get.varietiesInfo.data';
    var field = JSON.stringify({loginId:req.session.user.id,"pageNumber":page,"pageSize":7});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,api.api.varieties.list,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                var list = data.invoke_data;
                return res.json(list);
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                return res.json(data);
            }
        });
    });
};

// == 添加品种信息
exports.addVariety = function (req,res) {
    var _add = req.body.add;
    _add.loginId = req.session.user.id;
    var field = JSON.stringify(_add);
    var method = 'get.addVarietiesInfo.data';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,api.api.varieties.add,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                return res.json(data);
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                return res.json(data);
            }
        });
    });
};

// == 编辑品种信息
exports.editVariety = function (req,res) {
    var varietyId = req.body.varietyId;
    var field = JSON.stringify({loginId:req.session.user.id,varietyId:varietyId});
    var method = 'get.variety.info';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,api.api.varieties.edit,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                return res.json(data);
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                return res.json(data);
            }
        });
    });
};

// == 更新品种信息
exports.updateVariety = function (req,res) {
    var _update = req.body.update;
    _update.loginId = req.session.user.id;
    var field = JSON.stringify(_update);
    var method = 'get.editVarietiesInfo.data';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,api.api.varieties.update,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                return res.json(data);
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                return res.json(data);
            }
        });
    });
};

// == 删除品种信息
exports.deleteVariety = function (req,res) {
    var varietyId = req.body.varietyId;
    var field = JSON.stringify({loginId:req.session.user.id,varietyId:varietyId});
    var method = 'get.deleteVarietiesInfo.data';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,api.api.varieties.delete,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                return res.json(data);
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                return res.json(data);
            }
        });
    });
};

// == 品种信息表单验证
exports.formValidity = function (req,res) {
    var varietiesName = req.body.varietiesName;
    var method = 'check.variety.name';
    var field = JSON.stringify({varietiesName:varietiesName});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,api.api.varieties.form.varietyname,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                return res.json(data);
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                return res.json(data);
            }
        });
    });
};
