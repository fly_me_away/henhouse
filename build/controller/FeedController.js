var API = require('../config/api');
var UtilService = require('../service/UtilService');

// == 品牌简介添加页
exports.addPage = function (req,res) {
    var method = 'get.editFeed.data';
    var field = JSON.stringify({loginId:req.session.user.id,id:1});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.feed.view,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.render('./feed/add',{
                    title:"饲料信息",
                    feed:data.feed
                });
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};

// == 添加饲料信息
exports.addItem = function (req,res) {
    var add = req.body.add;
    var method = 'get.updateFeed.data';
    var field = JSON.stringify({loginId:req.session.user.id,feed:add.feed});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.feed.add,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.redirect('/feed')
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};
