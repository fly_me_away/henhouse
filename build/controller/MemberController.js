var API = require('../config/api');
var UtilService = require('../service/UtilService');
// == 会员列表
exports.listPage = function (req,res) {
    var page = req.params.page || 1;
    var method = 'get.seeMember.data';
    var field = JSON.stringify({loginId:req.session.user.id,pageNumber:page,pageSize:7});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.member.list,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                res.render('./member/list',{
                    title:"会员列表",
                    page:data.invoke_data.page,
                    data:data.invoke_data.list
                });
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                res.redirect('/404');
            }
        });
    });
};

// == 添加会员
exports.addMember = function (req,res) {
    res.render('./member/add',{
        title:"添加会员"
    });
};

// == 会员添加鸡舍
exports.addMemberHenhouse = function (req,res) {
    var page = req.params.page || 1;
    var breederId = req.query.breederId || '';
    var status = req.query.status || '';
    var hencoopNumber = req.query.hencoopNumber || '';
    var dayAge = req.query.dayAge || '';
    var varieties = req.query.varieties || '';
    var memberId = req.query.memberId || '';
    var method = 'get.chickenList.operation';
    var field = JSON.stringify({loginId:req.session.user.id,pageNumber:page,pageSize:6,memberId:memberId,breederId:breederId,dayAge:dayAge,varieties:varieties,status:status,hencoopNumber:hencoopNumber});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.member.addhenhouse,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                res.render('./member/addhenhouse',{
                    title:"会员添加鸡舍",
                    page:data.page,
                    data:data.invoke_data,
                    breeders:data.invoke_data_breeders,
                    varieties:data.invoke_data_varieties,
                    henhouseIds:data.invoke_data_hasWorkHencoopsIds
                });
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                res.redirect('/404');
            }
        });
    });
};

// == 会员表单验证
exports.formValidity = function (req,res) {
    var username = req.body.username;
    var method = 'check.member.username';
    var field = JSON.stringify({userName:username});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.member.form.username,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                return res.json(data);
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                return res.json(data);
            }
        });
    });
};

// == 保存会员
exports.saveMember = function (req,res) {
    var _save = req.body.save;
    _save.loginId = req.session.user.id;
    _save.type = 3;
    var field = JSON.stringify(_save);
    var method = 'get.addMember.data';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.member.save,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                return res.json(data);
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                return res.json(data);
            }
        });
    });
};

// == 删除会员
exports.deleteMember = function (req,res) {
    var memberid = req.body.memberId;
    var field = JSON.stringify({loginId:req.session.user.id,memberId:memberid});
    var method = 'get.deleteMemberInfo.data';
    console.log(field);
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.member.delete,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                return res.json(data);
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                return res.json(data);
            }
        });
    });
};

// == 查看会员
exports.viewMember = function (req,res) {
    var memberid = req.params.id;
    var field = JSON.stringify({loginId:req.session.user.id,memberId:memberid});
    var method = 'get.seeMemberInfo.data';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.member.view,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                res.render('./member/view',{
                    title:"查看会员",
                    data:data.invoke_data,
                    buyhencoopTime:UtilService.dateFormat(data.invoke_data.buyhencoopTime.time),
                    hencoopOutTime:UtilService.dateFormat(data.invoke_data.hencoopOutTime.time),
                    hencoopNumber:data.invoke_data_hencoops
                });
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                res.redirect('/404');
            }
        });
    });
};

// == 编辑会员
exports.editMember = function (req,res) {
    var memberid = req.query.memberId || '';
    var field = JSON.stringify({loginId:req.session.user.id,memberId:memberid});
    var method = 'get.seeMemberInfo.data';
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.member.view,function (data) {
            if (data.invoke_result == "INVOKE_SUCCESS"){
                res.render('./member/edit',{
                    title:"编辑会员",
                    data:data.invoke_data,
                    buyhencoopTime:UtilService.dateFormat(data.invoke_data.buyhencoopTime.time),
                    hencoopOutTime:UtilService.dateFormat(data.invoke_data.hencoopOutTime.time),
                    hencoopNumber:data.invoke_data_hencoops
                });
            }else if(data.invoke_result == "INVOKE_FAILURE"){
                res.redirect('/404');
            }
        });
    });
};
