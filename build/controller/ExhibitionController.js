var API = require('../config/api');
var fs = require('fs');
var path = require('path');
var UtilService = require('../service/UtilService');

// == 展示列表
exports.listItem = function (req,res) {
    var page = req.params.page || 1;
    var method = 'get.exhibitionList.data';
    var field = JSON.stringify({loginId:req.session.user.id,pageNumber:page,pageSize:7});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.exhibition.list,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.render('./exhibition/list',{
                    title:"展示",
                    page:data.invoke_data.page,
                    data:data.invoke_data.list,
                    hencoop:data.invoke_data.hencooplist
                });
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};

// == 添加视频
exports.uploadVideo = function (req,res,next) {
    var uploadVideo = req.files.videofile;
    var filePath = uploadVideo.path;
    var originalFileName = uploadVideo.originalFilename;
    // 判断是否传入图片
    if (originalFileName){
        var farmImagePath = path.join(__dirname,'../../','public/src/update/','farmerid'+req.session.user.id+'/');
        fs.exists(farmImagePath,function (exists) {
            if(exists){
                fs.readFile(filePath,function (err,data) {
                    var timestamp = Date.now();
                    var type = uploadVideo.type.split('/')[1];
                    var image = timestamp+'.'+type;
                    var newPath = farmImagePath+image;
                    fs.writeFile(newPath,data,function (err) {
                        req.videoPath = '/update/farmerid'+req.session.user.id+'/'+image;
                        next();
                    });
                });
            }else {
                fs.mkdir(farmImagePath,function (err) {
                    if(err)
                        throw err;
                    fs.readFile(filePath,function (err,data) {
                        var timestamp = Date.now();
                        var type = uploadVideo.type.split('/')[1];
                        var image = timestamp+'.'+type;
                        var newPath = farmImagePath+image;

                        fs.writeFile(newPath,data,function (err) {
                            req.imagePath = '/update/farmerid'+req.session.user.id+'/'+image;
                            next();
                        });
                    });
                });
            }
        });
    }else{
        next();
    }
};

// == 展示添加页
exports.addPage = function (req,res) {
    var page = req.params.page || 1;
    var method = 'get.exhibitionList.data';
    var field = JSON.stringify({loginId:req.session.user.id,pageNumber:page,pageSize:7});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.exhibition.list,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.render('./exhibition/add',{
                    title:"添加展示信息",
                    hencoop:data.invoke_data.hencooplist
                });
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};

// == 添加展示信息
exports.addItem = function (req,res) {
    if (req.imagePath) {
        var imagePath = 'http://' + API.url.hostname + ':3000' + req.imagePath;
    }else{
        var imagePath = req.body.oldposterfile || '';
    }
    if (req.videoPath) {
        var videoPath = 'http://' + API.url.hostname + ':3000' + req.videoPath;
    }else{
        var videoPath = req.body.oldvideofile || '';
    }
    var add = req.body.add;
    var method = 'get.addExhibition.data';
    var field = JSON.stringify({loginId:req.session.user.id,hencoop_id:add.hencoop_id,title:add.exhibition_title,content:add.exhibition_content,exhibition_video:videoPath,exhibition_poster:imagePath});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.exhibition.add,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.redirect('/exhibition/list/1')
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};

// == 删除展示信息
exports.deleteItem = function (req,res) {
    var id = req.body.id;
    var method = 'get.deleteExhibition.data';
    var field = JSON.stringify({loginId:req.session.user.id,id:id});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.exhibition.del,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                return res.json(data);
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                return res.json(data);
            }
        });
    });
};

// == 编辑展示信息
exports.editPage = function (req,res) {
    var id = req.params.id;
    var method = 'get.edit.data';
    var field = JSON.stringify({loginId:req.session.user.id,id:id});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.exhibition.editShow,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.render('./exhibition/edit',{
                    title:"编辑展示信息",
                    exhid:data.exhibition_hencoop.id,
                    exhtitle:data.exhibition_hencoop.title,
                    exhcontent:data.exhibition_hencoop.content,
                    hencoop:data.hencooplist,
                    exhvideo:data.exhibition_hencoop.exhibition_video || '',
                    exhposter:data.exhibition_hencoop.exhibition_poster || ''
                });
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};

// == 修改展示信息
exports.updateItem = function (req,res) {
    if (req.imagePath) {
        var imagePath = 'http://' + API.url.hostname + ':3000' + req.imagePath;
    }else{
        var imagePath = req.body.oldposterfile || '';
    }
    if (req.videoPath) {
        var videoPath = 'http://' + API.url.hostname + ':3000' + req.videoPath;
    }else{
        var videoPath = req.body.oldvideofile || '';
    }
    var edit = req.body.edit;
    var method = 'get.updateExhibition.data';
    var field = JSON.stringify({loginId:req.session.user.id,id:edit.exhibition_id,hencoop_id:edit.hencoop_id,title:edit.exhibition_title,content:edit.exhibition_content,exhibition_video:videoPath,exhibition_poster:imagePath});
    UtilService.getSign(method,function (sign) {
        UtilService.getDataApi(method,field,sign,API.api.exhibition.update,function (data) {
            if (data.invoke_result == 'INVOKE_SUCCESS'){
                res.redirect('/exhibition/list/1')
            }else if(data.invoke_result == 'INVOKE_FAILURE'){
                res.redirect('/404');
            }
        });
    });
};