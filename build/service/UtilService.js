var api = require('../config/api');
var http = require('http');
var qs = require('querystring');
var moment = require('moment');

var UtilService = (function () {
    /*
    * http请求接口
    * @param method 请求数据方法
    * @param field field请求域
    * @param sign 签名
    * @param path 请求地址
    * @param callback 回调函数 */
    var getDataApi = function (method,field,sign,path,callback) {
        var data = api.config;
        data.method = method;
        data.field = field;
        data.sign = sign;
        console.log("/***************************请求数据***********************************/");
        console.log(data.field);
        console.log("/---------------------------------------------------------------------/");
        var content = qs.stringify(data);

        var options = {
            hostname: api.url.hostname,
            port: api.url.port,
            path: path,
            method: 'POST',
            headers: api.url.headers
        };
        var req = http.request(options, function (res) {
            var jsons = '';
            res.setEncoding('utf8');
            res.on('data', function (chunk) {
                console.log("/*********************************返回结果********************************/");
                console.log(chunk);
                console.log("/-----------------------------------------------------------------------/");
                jsons += chunk; //json传入不完整
            });
            res.on('end', function(){
                var data;
                try{
                    data = JSON.parse(jsons);
                }catch(e) {
                    console.log("接口数据异常"+e);
                    console.log(data);
                    data = {"invoke_result":"INVOKE_ERROR","invoke_data":e};
                }
                return callback(data);
            });
        });
        req.on('error', function (e) {
            console.log('problem with request: ' + e.message);
        });
// write data to request body
        req.write(content);
        req.end();
    };
    /* json转字符串获取签名
     * param josn object | 签名字符串*/
    var jsonToString = function (json) {
        var str = "";
        for(var param in json){
            str += param+"="+json[param]+"&";
        }
        return str.substring(0,str.length-1);
    };
    /* 签名方法
    * */
    var getSign = function (method,callback) {
        var strSign = api.sign;
            strSign.method = method;
        var data = {
            content:jsonToString(strSign)
        };
        var content = qs.stringify(data);

        var options = {
            hostname: api.url.hostname,
            port: api.url.port,
            path: api.api.sign,
            method: 'POST',
            headers: api.url.headers
        };
        var req = http.request(options, function (res) {
            res.setEncoding('utf8');
            res.on('data', function (chunk) {
                var getSignData = JSON.parse(chunk);
                if(getSignData.invoke_result === "INVOKE_SUCCESS"){
                    return callback(getSignData.invoke_data);
                }else{
                    console.log("签名错误！！！");
                }
            });
        });
        req.on('error', function (e) {
            console.log('problem with request: ' + e.message);
        });
// write data to request body
        req.write(content);

        req.end();
    };
    /*
    * 时间格式化*/
    var dateFormat = function (time) {
        moment.locale('zh_cn');
        return moment(time).format('YYYY-MM-DD HH:mm')
    };
    /*
    * 字符串或数组转json*/
    var arrToJson = function (arr1,arr2) {
        if (typeof arr1 === "string" && typeof arr2 === "string"){
            arr1 =[arr1],arr2 = [arr2];
        }
        var obj = {};
        var str = "";
        if(arr1.length === arr2.length){
            for (var i = 0;i<arr1.length;i++){
                obj['task'] = arr1[i];
                obj['workTime'] = arr2[i];
                str += JSON.stringify(obj)+","
            }
            return str.substring(0,str.length-1)
        }else {
            console.log("传入数据错误")
        }

    };
    return{
        getSign:getSign,
        getDataApi:getDataApi,
        dateFormat:dateFormat,
        arrToJson:arrToJson
    }
})();
module.exports = UtilService;

