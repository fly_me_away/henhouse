var WindowController = (function ($) {
    var setIframeSize = function () {
        var getWindowWidth = $(window).width();
        var getWindowHeight = $(window).height();
        return{
            getWindowWidth:getWindowWidth,
            getWindowHeight:getWindowHeight
        }
    };

    return{
        setIframeSize:setIframeSize
    }
})(jQuery);
module.exports = WindowController;
