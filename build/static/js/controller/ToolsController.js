var ToolsController = (function ($) {
    var getDate = function () {
        var monthNames = ["1月", "2月", "3月", "4月", "5月", "6月","7月", "8月", "9月", "10月", "11月", "12月"];
        var dayNames = ["星期日, ", "星期一, ", "星期二, ", "星期三, ", "星期四, ", "星期五, ", "星期六, "];
        var newDate = new Date();
        newDate.setDate(newDate.getDate());
        return {
            week:dayNames[newDate.getDay()],
            date:newDate.getFullYear() + '年 ' + monthNames[newDate.getMonth()] + ' ' + newDate.getDate()+'日',
            weekAndDate:dayNames[newDate.getDay()] + " " + newDate.getFullYear() + '年 ' + monthNames[newDate.getMonth()] + ' ' + newDate.getDate()+'日'
        }
    };

    var createDate = function (date) {
        return moment(date).format('L');
    };

    var getToday = function () {
        var today = new Date();
        return today.getFullYear()+"-"+(today.getMonth()+1)+"-"+today.getDate();
    };

    // == 获取url中的参数值
    /*
     * @param name String /detail.html?topicId=4
     * */
    var getQueryString = function (name) {
        var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
        var r = window.location.search.substr(1).match(reg);
        if (r != null) {
            return decodeURIComponent(r[2]);
        }
        return null;
    };

    /*
     * @param images String /图片地址为null，转换为默认图片地址
     * */
    var textDefault = function (text,str) {
        if(text == null || text == ''){
            if(str){
                return str;
            }else{
                return "";
            }
        }else{
            return text;
        }
    };

    return{
        getDate:getDate,
        createDate:createDate,
        getQueryString:getQueryString,
        textDefault:textDefault,
        getToday:getToday
    }
})(jQuery);
module.exports = ToolsController;
