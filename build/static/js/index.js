var Api = require('../../config/api');
var WindowController = require('./controller/WindowController');
var ToolsController = require('./controller/ToolsController');
var FormController = require('./controller/FormController');

window.Hen = {
        WindowController:WindowController,
        ToolsController:ToolsController,
        Form:FormController,
        Api:Api
    };

// *************** 公共方法 **************** //
// == 当发起ajax请求前添加加载器
$(document).ajaxSend(function(event, request, settings) {

});
// == 当ajax请求完成后关闭加载器
$(document).ajaxComplete(function(event, request, settings) {
});
// == 网络请求错误提示
$(document).ajaxError(function(event, request, settings) {
   // window.location.href = "../../page/404.html";
});
// *************** 模态框公用方法 **************//
// *************** 表单公用方法 ***************//
$(document).ready(function () {
    $('.create-date').val(ToolsController.getDate().date);
});
