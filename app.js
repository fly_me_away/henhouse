var API = require('./build/config/api');
var path = require('path');
var express  = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var ueditor = require('ueditor');
var multipart = require('connect-multiparty');
var app = express();

// == 模板引擎
app.set('views','./build/views');
app.set('view engine','jade');
app.set('trust proxy',false);

app.use(bodyParser.urlencoded({extended: true}));
// == 静态资源
app.use(express.static('./public/src'));
// == 设置session
app.use(cookieParser());
app.use(session({
    secret:'henhouse',
    cookie:{maxAge:60*60*1000,secure: false},
    resave:false,
    saveUninitialized: true
}));
app.locals.moment = require('moment');

//== 富文本编辑器配置
app.use("/ueditor/ue", ueditor(path.join(__dirname, './public/src'), function(req, res, next) {
    var ActionType = req.query.action;
    if (ActionType === 'uploadimage' || ActionType === 'uploadfile' || ActionType === 'uploadvideo') {
        var file_url = '/update/images/';//默认上传地址为图片
        /*其他上传格式的地址*/
        if (ActionType === 'uploadfile') {
            file_url = '/update/images/'; //附件保存地址
        }
        if (ActionType === 'uploadvideo') {
            file_url = '/update/videos/'; //视频保存地址
        }
        res.ue_up(file_url); //你只要输入要保存的地址 。保存操作交给ueditor来做
        res.setHeader('Content-Type', 'text/html');
    }else if(req.query.action === 'listimage'){
        var img_url = '/update/images/';
        res.ue_list(img_url);
    }else{
        res.setHeader('Content-Type', 'application/json');
        res.redirect('/ueditor/nodejs/config.json')
    }
}));

// == 文件上传 注：在富文本编辑器后引入，否则将拦截富文本编辑器的上传操作
app.use(multipart());

// == 路由
var routes = require('./build/config/routes');
routes(app);


app.listen(3000,function () {
    console.log('server listening '+ API.url.hostname+":"+API.url.port);
});