var path = require('path');

var ROOT_PATH = path.resolve(__dirname);
var ENTER_PATH = path.resolve(ROOT_PATH,'build/static/js');
var OUTPUT_PATH = path.resolve(ROOT_PATH,'public/src/js');

module.exports = {
    entry:ENTER_PATH,
    output:{
        path:OUTPUT_PATH,
        filename:'henhouse.js'
    }
};